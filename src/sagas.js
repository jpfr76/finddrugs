import { fork } from 'redux-saga/effects';

import productSagas from './product/sagas';

export default function* () {
    yield fork(productSagas);
}