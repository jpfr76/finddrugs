import { createAction } from 'redux-actions';

export const SET_DRUG_NAME = 'SET_DRUG_NAME';
export const setDrugName = createAction(
    SET_DRUG_NAME,
    (name) => ({ name })
);

export const GET_PRODUCT_LIST_REQUEST = 'GET_PRODUCT_LIST_REQUEST';
export const getProductListRequest = createAction(GET_PRODUCT_LIST_REQUEST);
export const GET_PRODUCT_LIST_SUCCESS = 'GET_PRODUCT_LIST_SUCCESS';
export const getProductListSuccess = createAction(GET_PRODUCT_LIST_SUCCESS);
export const GET_PRODUCT_LIST_FAILURE = 'GET_PRODUCT_LIST_FAILURE';
export const getProductListFailure = createAction(GET_PRODUCT_LIST_FAILURE);

