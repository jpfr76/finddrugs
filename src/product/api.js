import axios from "axios/index";
import _ from "underscore";

export const fetchDrugList = (name) => {
    let axiosConfig = {
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    };
    return axios
        .get(`https://rxnav.nlm.nih.gov/REST/drugs?name=${name}`, axiosConfig)
        .then(response => {
            if (!response.data) {
                return [];
            }
            return _.chain(response.data.drugGroup.conceptGroup)
                .filter(g => !_.isEmpty(g.conceptProperties))
                .map(g => {
                    return _.map(g.conceptProperties, p => {
                        return {
                            rxcui: p.rxcui,
                            name: p.name,
                            synonym: p.synonym,
                            tty: p.tty,
                            language: p.language,
                            suppress: p.suppress,
                            umlscui: p.umlscui
                        };
                    });
                })
                .flatten()
                .value();
        })
        .catch(error => console.log(error));
};
