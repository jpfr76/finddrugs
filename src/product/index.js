import React from 'react';
import Filter from './Filter';
import DrugTable from './DrugTable';
import { getProductListRequest as getProductListRequestAction, setDrugName as setDrugNameAction} from './actions';
import { connect } from 'react-redux';

const ProductSearch = ({setDrugName, productFilterByDrugName, productList, drugName}) => {
    return (
        <div>
            <div>
                <Filter
                    drugName={drugName}
                    setDrugName={setDrugName}
                    productFilterByDrugName={productFilterByDrugName}/>
            </div>
            <div>
                <DrugTable data={productList} />
            </div>
        </div>
    );
};

const mapStateToProps = state => state.product;
const mapDispatchToProps = {
    setDrugName: setDrugNameAction,
    productFilterByDrugName: getProductListRequestAction
};
export default connect(mapStateToProps, mapDispatchToProps)(ProductSearch);
