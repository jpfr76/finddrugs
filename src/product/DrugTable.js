import React from 'react';
import ReactTable from "react-table";
import "react-table/react-table.css";

const DrugTable = (props) => {
    let columnDefs = [
        {
            Header: "Id",
            accessor: "rxcui",
            maxWidth: 80
        },
        {
            Header: "Name",
            accessor: "name",
            width: "100%"
        }
    ];
    return (
        <ReactTable data={props.data}
                    columns={columnDefs}
                    defaultPageSize={10}
                    className="-striped -highlight" />
    );
};

export default DrugTable;