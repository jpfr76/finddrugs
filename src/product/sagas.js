import { takeEvery } from 'redux-saga';
import { call, put } from 'redux-saga/effects';

import { fetchDrugList as fetchDrugListApi } from './api';
import { GET_PRODUCT_LIST_REQUEST, getProductListSuccess, getProductListFailure } from './actions';

function* getProductList({payload}) {
    try {
        const result = yield call(fetchDrugListApi, payload);
        yield put(getProductListSuccess(result));
    } catch (err) {
        yield put(getProductListFailure());
    }
}

export default function* sagas() {
    yield takeEvery(GET_PRODUCT_LIST_REQUEST, getProductList)
}
