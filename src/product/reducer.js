import {
    SET_DRUG_NAME,
    PRODUCT_FILTER_BY_DRUG_NAME,
    GET_PRODUCT_LIST_REQUEST,
    GET_PRODUCT_LIST_SUCCESS,
    GET_PRODUCT_LIST_FAILURE
} from './actions';

const initialState = {
    drugName: '',
    filter: false
};

export default (state = initialState, { type, payload }) => {
    state = {...state, filter: false};
    switch (type) {
        case SET_DRUG_NAME: {
            return { ...state, drugName: payload.name };
        }
        case GET_PRODUCT_LIST_REQUEST: {
            return { ...state, loadingProductList: true};
        }
        case GET_PRODUCT_LIST_SUCCESS: {
            return { ...state, productList: payload, loadingProductList: false};
        }
        case GET_PRODUCT_LIST_FAILURE: {
            return { ...state, loadingProductList: false};
        }
        default:
            return state;
    }
};
