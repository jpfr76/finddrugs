import React from 'react';
import Form from 'react-bootstrap/lib/Form';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import FormControl from 'react-bootstrap/lib/FormControl';
import Col from 'react-bootstrap/lib/Col';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';
import Button from 'react-bootstrap/lib/Button';

const Filter = (props) => {
    return (
        <Form horizontal>
            <FormGroup controlId="formHorizontalName">
                <Col componentClass={ControlLabel} sm={1}>Name</Col>
                <Col sm={11}>
                    <FormControl type="text" placeholder="Filter" value={props.drugName}
                    onChange={event => props.setDrugName(event.target.value)}/>
                </Col>
            </FormGroup>
            <FormGroup>
                <Col smOffset={1} sm={11} >
                    <Button className="pull-left" bsStyle="primary" type="submit"
                            onClick={(e) => {
                                e.preventDefault();
                                props.productFilterByDrugName(props.drugName);
                            }}>Search</Button>
                </Col>
            </FormGroup>
        </Form>
    );
};

export default Filter;